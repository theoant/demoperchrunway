<div class="container-fluid footer">
    <div class="container">
      <div class="row cards-row">
        <div class="col-lg-3 mt-5 footer-menu">
          <a class="navbar-brand" href="index.php"><img src="images/logo.png" alt="logo" width="150" height="50"></a>
        </div>
        <div class="col-lg-3 mt-5 footer-menu">
          <h5 class="mb-3">Our Services</h5>
          <a href="fit-to-fly.php"><p>Fit to Fly</p></a>
          <a href="test-to-release.php"><p>Test to Release</p></a>
          <a href="antibody-tests.php"><p>Antibody Tests</p></a>
          <a href="home-testing.php"><p>Home Testing</p></a>
          <a href="business.php"><p>For Businesses</p></a>
        </div>
        <div class="col-lg-3 mt-5 footer-menu">
          <h5 class="mb-3">Company</h5>
          <a href="about.php"><p>About Us</p></a>
          <!-- <a href=""><p>COVID Travel calculator</p></a> -->
          <a href="blog.php"><p>Blog</p></a>
          <a href="contact.php"><p>Contact Us</p></a>
          <a href="privacy.php"><p>Privacy Policy</p></a>
		  <a href="perch/resources/terms-nlclinic.pdf"><p>Terms & Conditions</p></a>		  		  
        </div>
        <div class="col-lg-3 mt-5 footer-menu">
          <h5 class="mb-3">Follow Us</h5>
          <div class="social-media">
            <a href="https://www.facebook.com/NL-Clinic-Peterborough-103434025123889"><img src="images/facebook.png" class="mr-3" width="20" height="30"></a>
            <a href="https://www.instagram.com/nlclinicpeterborough/"><img src="images/instagram.png" class="mr-3" width="30" height="30"></a>
            <a href="https://www.linkedin.com/company/nl-clinic-peterborough/"><img src="images/linkedin.png" class="mb-2" width="35" height="35"></a>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  </body>
  
  </html>