<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TZLQSTB"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<nav class="navbar navbar-expand-lg navbar-dark bg-light">
  <div class="container-fluid">
  <a class="navbar-brand" href="index.php"><img src="images/logo.png" alt="logo" class="logo ml-xl-5"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        <a class="nav-link" href="fit-to-fly.php">Fit to Fly</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="test-to-release.php">Test to Release</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="antibody-tests.php">Antibody Tests</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="home-testing.php">2 & 8 Day Tests</a>
      </li>
      <!-- <li class="nav-item">
        <a class="nav-link" href="blog.php">Blog<a>
      </li> -->
        <!-- <div class="line1"></div> -->
      <!-- <span class="float-right"> -->
        
      <!-- </span> -->
    </ul>

    <ul class="navbar-nav ml-auto">
        <li class="nav-item border-left">
          <a class="nav-link" href="contact.php"><img src="images/phone.png" class="ml-xl-2" width="25" height="20"></a>
        </li>
        <li class="nav-item border-right">
          <a class="nav-link" href="contact.php"><img src="images/mail.png" class="mr-1 mail" width="25" height="20"></a>
        </li>
        <!-- <div class="line2"></div> -->
        <li class="nav-item ml-lg-3">
        <?php $pathInPieces = explode('/', $_SERVER['REQUEST_URI']);
        if (strpos($pathInPieces[1], 'landing') !== 0) {
              ?>
        <a href="book-now.php" class="btn btn-primary">Book Now</a>
        <?php } ?>
        </li>
    </ul>
  </div>
  </div>
</nav>
