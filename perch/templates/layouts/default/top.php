<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?php perch_pages_title(); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Merriweather&display=swap" rel="stylesheet">
    <style>
        body {
            font-family: Merriweather, sans-serif;
        }
    </style>
    <?php perch_page_attributes(); ?>
</head>
<body>
<?php
    perch_pages_navigation([
                               'levels' => 1,
                           ]);
?>
