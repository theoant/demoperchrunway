<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?php perch_pages_title(); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Merriweather&display=swap" rel="stylesheet">
    <style>
        body {
            margin: 0;
            padding: 0;
        }

        .container {
            height: 80vh;
            width: 100vw;
            display: flex;
            align-items: center;
            justify-content: center;
        }

        .error {
            font-family: Merriweather, sans-serif;
            padding: 4rem 8rem;
        }
    </style>
</head>
<body>
    <div class="container">
        <section class="error">