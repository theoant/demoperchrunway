<?php
    perch_layout('errors/top');
    perch_wrap('h1', 'Sorry, that page could not be found.');
    perch_content('Error description');
    perch_layout('errors/bottom');
