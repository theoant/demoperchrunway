<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
	<title><?php perch_pages_title(); ?></title>
	<?php perch_page_attributes(); ?>
</head>
<body>
    <?php perch_content('Region 1'); ?>
    <?php perch_content('Region 2'); ?>
    <?php perch_content('Region 3'); ?>

</body>
</html>