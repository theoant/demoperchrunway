<?php

	// Prevent running directly:
    if (!defined('PERCH_DB_PREFIX')) exit;

	/*$sql = "

		ALTER TABLE `__PREFIX__members` CHANGE `memberPassword` `memberPassword` CHAR(255)  NULL  DEFAULT NULL;

	";*/

	$sql = "
    		        CREATE TABLE `__PREFIX__members_member_notes` (
                      `memberID` int(10) NOT NULL,
                      `noteID` int(10) NOT NULL,
                      `noteDate` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                      `addedBy` char(40) NOT NULL DEFAULT '',
                       PRIMARY KEY (`memberID`,`noteID`)
                       ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

                           CREATE TABLE `__PREFIX__members_notes` (
                                     `noteID` int(10) unsigned NOT NULL AUTO_INCREMENT,
                                     `note` char(255) NOT NULL DEFAULT '',
                                     PRIMARY KEY (`noteID`)
                                   ) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

    	";

	$sql = str_replace('__PREFIX__', PERCH_DB_PREFIX, $sql);

	$DB = PerchDB::fetch();

    $statements = explode(';', $sql);
    foreach($statements as $statement) {
        $statement = trim($statement);
        if ($statement!='') $DB->execute($statement);
    }

    $Settings->set('perch_members_update', '1.6.5');
