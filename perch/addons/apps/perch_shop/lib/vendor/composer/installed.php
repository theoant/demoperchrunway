<?php return array(
    'root' => array(
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../../',
        'aliases' => array(),
        'reference' => '53ef96ed2e5f7aa5bc66ed81a1de7b1378c20cad',
        'name' => '__root__',
        'dev' => true,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../../',
            'aliases' => array(),
            'reference' => '53ef96ed2e5f7aa5bc66ed81a1de7b1378c20cad',
            'dev_requirement' => false,
        ),
        'braintree/braintree_php' => array(
            'pretty_version' => '3.16.0',
            'version' => '3.16.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../braintree/braintree_php',
            'aliases' => array(),
            'reference' => 'd6ce80f7f54abe9972d577173343d1975bc49e77',
            'dev_requirement' => false,
        ),
        'guzzle/batch' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/cache' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/common' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/guzzle' => array(
            'pretty_version' => 'v3.9.3',
            'version' => '3.9.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../guzzle/guzzle',
            'aliases' => array(),
            'reference' => '0645b70d953bc1c067bbc8d5bc53194706b628d9',
            'dev_requirement' => false,
        ),
        'guzzle/http' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/inflection' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/iterator' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/log' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/parser' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/plugin' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/plugin-async' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/plugin-backoff' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/plugin-cache' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/plugin-cookie' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/plugin-curlauth' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/plugin-error-response' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/plugin-history' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/plugin-log' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/plugin-md5' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/plugin-mock' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/plugin-oauth' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/service' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/stream' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzlehttp/guzzle' => array(
            'pretty_version' => '7.3.0',
            'version' => '7.3.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../guzzlehttp/guzzle',
            'aliases' => array(),
            'reference' => '7008573787b430c1c1f650e3722d9bba59967628',
            'dev_requirement' => false,
        ),
        'guzzlehttp/promises' => array(
            'pretty_version' => '1.4.1',
            'version' => '1.4.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../guzzlehttp/promises',
            'aliases' => array(),
            'reference' => '8e7d04f1f6450fef59366c399cfad4b9383aa30d',
            'dev_requirement' => false,
        ),
        'guzzlehttp/psr7' => array(
            'pretty_version' => '2.0.0',
            'version' => '2.0.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../guzzlehttp/psr7',
            'aliases' => array(),
            'reference' => '1dc8d9cba3897165e16d12bb13d813afb1eb3fe7',
            'dev_requirement' => false,
        ),
        'ircmaxell/password-compat' => array(
            'pretty_version' => 'v1.0.4',
            'version' => '1.0.4.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../ircmaxell/password-compat',
            'aliases' => array(),
            'reference' => '5c5cde8822a69545767f7c7f3058cb15ff84614c',
            'dev_requirement' => false,
        ),
        'league/csv' => array(
            'pretty_version' => '7.2.0',
            'version' => '7.2.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../league/csv',
            'aliases' => array(),
            'reference' => '69bafa6ff924fbf9effe4275d6eb16be81a853ef',
            'dev_requirement' => false,
        ),
        'omnipay/authorizenet' => array(
            'pretty_version' => '2.4.2',
            'version' => '2.4.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../omnipay/authorizenet',
            'aliases' => array(),
            'reference' => '6e1990f5d22f0f8e4dfe363b89c9776d0d803c34',
            'dev_requirement' => false,
        ),
        'omnipay/braintree' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'type' => 'library',
            'install_path' => __DIR__ . '/../omnipay/braintree',
            'aliases' => array(
                0 => '2.0.x-dev',
            ),
            'reference' => '54846f2d148b2b6163a37b05d19bf5479c2bf855',
            'dev_requirement' => false,
        ),
        'omnipay/common' => array(
            'pretty_version' => 'v2.5.1',
            'version' => '2.5.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../omnipay/common',
            'aliases' => array(),
            'reference' => '731386e54688405a475d6dd43cd6397728cd709e',
            'dev_requirement' => false,
        ),
        'omnipay/manual' => array(
            'pretty_version' => 'v2.2.0',
            'version' => '2.2.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../omnipay/manual',
            'aliases' => array(),
            'reference' => 'db31b81dc3a9ccbc61a805dd9f922b7bfd0eb0e9',
            'dev_requirement' => false,
        ),
        'omnipay/paypal' => array(
            'pretty_version' => 'v2.6.0',
            'version' => '2.6.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../omnipay/paypal',
            'aliases' => array(),
            'reference' => '592f1cca058200adc76ed9599b13f21e81e01dda',
            'dev_requirement' => false,
        ),
        'omnipay/sagepay' => array(
            'pretty_version' => '2.3.1',
            'version' => '2.3.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../omnipay/sagepay',
            'aliases' => array(),
            'reference' => 'ca992b28a0d7ec7dbf218852dab36ec309dee07e',
            'dev_requirement' => false,
        ),
        'omnipay/stripe' => array(
            'pretty_version' => 'v2.4.1',
            'version' => '2.4.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../omnipay/stripe',
            'aliases' => array(),
            'reference' => '6eab75f838e11e2c951af52cf3939aeb37d11059',
            'dev_requirement' => false,
        ),
        'omnipay/worldpay' => array(
            'pretty_version' => 'v2.2',
            'version' => '2.2.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../omnipay/worldpay',
            'aliases' => array(),
            'reference' => '26ead4ca2c6ec45c9a3b3dad0be8880e0b1df083',
            'dev_requirement' => false,
        ),
        'psr/http-client' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-client',
            'aliases' => array(),
            'reference' => '2dfb5f6c5eff0e91e20e913f8c5452ed95b86621',
            'dev_requirement' => false,
        ),
        'psr/http-client-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'psr/http-factory' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-factory',
            'aliases' => array(),
            'reference' => '12ac7fcd07e5b077433f5f2bee95b3a771bf61be',
            'dev_requirement' => false,
        ),
        'psr/http-factory-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'psr/http-message' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-message',
            'aliases' => array(),
            'reference' => 'f6561bf28d520154e4b0ec72be95418abe6d9363',
            'dev_requirement' => false,
        ),
        'psr/http-message-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'ralouphie/getallheaders' => array(
            'pretty_version' => '3.0.3',
            'version' => '3.0.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../ralouphie/getallheaders',
            'aliases' => array(),
            'reference' => '120b605dfeb996808c31b6477290a714d356e822',
            'dev_requirement' => false,
        ),
        'stripe/stripe-php' => array(
            'pretty_version' => 'v3.23.0',
            'version' => '3.23.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../stripe/stripe-php',
            'aliases' => array(),
            'reference' => 'c702a2ed92f4b39603c4a796492d87bcc16c819c',
            'dev_requirement' => false,
        ),
        'symfony/event-dispatcher' => array(
            'pretty_version' => 'v2.8.12',
            'version' => '2.8.12.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/event-dispatcher',
            'aliases' => array(),
            'reference' => '889983a79a043dfda68f38c38b6dba092dd49cd8',
            'dev_requirement' => false,
        ),
        'symfony/http-foundation' => array(
            'pretty_version' => 'v2.8.12',
            'version' => '2.8.12.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/http-foundation',
            'aliases' => array(),
            'reference' => '91f87d27e9fe99435278c337375b0dce292fe0e2',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-mbstring' => array(
            'pretty_version' => 'v1.2.0',
            'version' => '1.2.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-mbstring',
            'aliases' => array(),
            'reference' => 'dff51f72b0706335131b00a7f49606168c582594',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php54' => array(
            'pretty_version' => 'v1.2.0',
            'version' => '1.2.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php54',
            'aliases' => array(),
            'reference' => '34d761992f6f2cc6092cc0e5e93f38b53ba5e4f1',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php55' => array(
            'pretty_version' => 'v1.2.0',
            'version' => '1.2.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php55',
            'aliases' => array(),
            'reference' => 'bf2ff9ad6be1a4772cb873e4eea94d70daa95c6d',
            'dev_requirement' => false,
        ),
    ),
);
