<?php define('PERCH_DEBUG', true);
    switch($_SERVER['SERVER_NAME']) {

        case 'localhost':
            include(__DIR__.'/config.localhost.php');
            break;

        default:
            include('config.production.php');
            break;
    }

    define('PERCH_LICENSE_KEY', 'R3-LOCAL-SQH220-PGG107-ALT019');
    define('PERCH_EMAIL_FROM', 'theodora@mooblu.com');
    define('PERCH_EMAIL_FROM_NAME', 'Theodora Antoniou');

    define('PERCH_LOGINPATH', '/perch');
    define('PERCH_PATH', str_replace(DIRECTORY_SEPARATOR.'config', '', __DIR__));
    define('PERCH_CORE', PERCH_PATH.DIRECTORY_SEPARATOR.'core');

    define('PERCH_RESFILEPATH', PERCH_PATH . DIRECTORY_SEPARATOR . 'resources');
    define('PERCH_RESPATH', PERCH_LOGINPATH . '/resources');
    
    define('PERCH_HTML5', true);
    define('PERCH_TZ', 'UTC');
